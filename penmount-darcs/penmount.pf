
# penmount driver for pf. this relies on the 'penmount' script, and
# 'driver.c', which will output device coordinates: integers between 0
# and 1024, with bottom left = 0,0

# what we need to do:
# 1. make the device behave as an ordinary event sender
# 2. map the coordinates to standard device coordinates

# part 1 is done using a task which calls 'responder'
# part 2 is done using calibration data computed by get-calibration.pf



# point A is (1/4, 1/4) and point B is (3/4, 3/4) in window coordinates.
"calibration.pf" load	 # defines pen-AX ...

# compute calibrator from point coordinates. this is a simple scaling + 
# translation:  outx = (sx * inx) + offsetx. scaling comes from the 
# distance between 2 points (we now it's 1/2), and offset by 
# substitution

# scaling
pen-BX pen-AX - 2. *  inverse constant pen-SX
pen-BY pen-AY - 2. *  inverse constant pen-SY

# offset
1. 4. /  pen-SX pen-AX *  -  constant pen-OX
1. 4. /  pen-SY pen-AY *  -  constant pen-OY

# perform coordinate transform:  pencoords -- windowcoords
: penmount->window-relative
	unpack
	  >float pen-SX * pen-OX + >r
	  >float pen-SY * pen-OY + r>
        2 pack ;

# process event so it will have window coordinates	
: apply-calibration 
  	split >r
	  penmount->window-relative
	r> unsplit ;

# task to read, calibrate and send event	
: spawn-penmount-reader
	1 fork		# pass 1 argument = the stream
	>r		# put it on the return stack
	begin
		r read-atom		# get next event
		"raw:" p dup p cr	# debug
		apply-calibration	# map it to window coordinates
		responder		# pass it to the responder
	again ;
	
# the events come from stdout of the 'penmount' script, which sets 
# serial port config and translates events to pf lists.

: open-penmount ("./penmount") "r" open-program ;


: start-penmount
	open-penmount 
	spawn-penmount-reader ;
